#!/usr/bin/bash

  ##############################################################################
 #                              Coded by Edge226
################################################################################
#
#               orbos-libtext - The Orbos text display engine.
#
################################################################################
#
#                                 Description:
# This library allows for text to be streamed to the display. It has two
# different modes that work in two entirely different ways to produce a similar
# comfort of reading at different speeds.
#
# One is called fastmsg and the other respectively called slowmsg.
#
# fastmsg will display your messages at 7.5 words a second or 450 words per
# minute, There is a pause at the end of each line for half a second.
#
# slowmsg will display each letter in a word at a rate of 12 per second for
# standard letters and 720 letters per minute. slowmsg has a grammar syntax
# coded into it. It will pause for half a second at a comma and three fourths of
# a second on a period. Using an average word length of 5 that would be
# 144 words per minute not including the grammar syntax, In reality the display
# will be much slower.
#
# I chose these message speeds simply due to personal testing and I felt that
# these were the times that the text felt like it flowed at its most fluid
# without being displayed too quickly.
#
# Messages sent to either of the text libraries will display exactly the same
# on the screen. Messages saved in files operate in a wysiwyg manner. How you
# format it in the text file will be exactly as it is displayed on screen.
#
################################################################################
#                                 -Usage notes-
#
# Make sure you have the right permissions:
#
#                                   Command:
#                        ******************************
#                       *  chmod a+rx ./orbos-libtext  *
#                        ******************************
#
# Optionally you can load the library into your shell so you can experiment with
#  different page layouts. You can also call the library in the same way
#  through a bash script.
#
#                                   Command:
#                         ****************************
#                        *     . ./orbos-libtext      *
#                         ****************************
#
# After you have done this you will be able to run the commands below.
# The last command only takes effect until the next reboot. Unless added to your
#  user or global bash start up scripts which is out of scope of this
#  documentation.
#
# If you want to set a default message speed at the top of a message file you
#  can create a msg function like so. This has the added benefit of being
#  shorter to type providing the benefit of increased speed and simplicity.
#
#                                   Functions:
#                      **********************************
#                     *  msg()                           *
#                    *   # This sets the message speed.   *
#                    *   # Fast is exponentially faster.  *
#                    *   # This prints a newline at the   *
#                    *   # end of each line.              *
#                    *   {                                *
#                    *     question="no"                  *
#                    *     #slowmsg "$@"                  *
#                    *
#                    *     fastmsg "$@"                   *
#                    *     #nodelaymsg "$@"               *
#                     *   }                              *
#                      **********************************
#
#                      **********************************
#                     *  qmsg()                           *
#                    *   # Meant for asking a question.   *
#                    *   # This sets the message speed.   *
#                    *   # Fast is exponentially faster.  *
#                    *   # No newlines.                   *
#                    *   {                                *
#                    *     question="yes"                 *
#                    *     #slowmsg "$@"                  *
#                    *     fastmsg "$@"                   *
#                    *     #nodelaymsg "$@"               *
#                     *   }                              *
#                      **********************************
#
 ###############################################################################

contimsg()
  ##############################################################################
 #
# Takes and uses no input.
# Prints a continue message and waits for input of a single character from
#  the user.
#
#                                   Example:
#                                 ************
#                                *  contimsg  *
#                                 ************
#
 ###############################################################################
{
  printf "\n"
  read -r -p "Press any key to continue." -n1 -s
  printf "\n\n"
}
libtextfunctions+=("contimsg")

rng()
## This function provides rng to a floating point number causing randomness in the pause function.
{
  modspeed="$1" #A floating point string is expected as the argument.
  if [[ "$modspeed" != "0" ]] && [[ "$rngseed" != "0" ]]
  # New lines use a 0.001 timing so we exclude that.
  then
    ((mid=rngseed/2)) # We divide it by 2 to find the mid point.
    varience=$(( (RANDOM % rngseed) )) # We int RNG to grab between 0-18.
    integer=${modspeed%%.*} ## We split our float into 2 integers.
    float=${modspeed##*.}
    floatnum=${#float} ## We track the float to see how many chracters it has.
    float=${float##*0} ## We remove everything before the last 0.
    if (( varience < mid ))
    # Here we check if the varience is low or high.
    # A low varience will be added to the float value.
    # A high varience will be converted to a single digit
    # that value is subtracted from the float value.
    then
      ((float=float+varience))
      floatnume=${#float}
    else
      ((varience=varience/2))
      if (( varience < float ))
      ## This if statement prevents the floating value from going
      ## into negative values.
      then
        ((float=float-varience))
      fi
      floatnume=${#float}
    fi
      for (( ; $floatnume < $floatnum ; floatnume++ ))
      # This prepends any 0's that may have been dropped by bash.
      do
        float="0$float" # This operation is really slow...
      done
    printf '%s%s%s' "$integer" "." "$float"
  else
    printf '%s' "$modspeed"
  fi
}
libtextvariables+=("rngseed" "modspeed" "varience" "integer" "float" "floatnum" "floatnume" )
libtextfunctions+=("rng")

pause()
  ##############################################################################
 #
# Pauses output for the ammount of seconds sent to it.
# Requires 1 variable which is expected to be a number.
#   - The number can be an integer or decimal value.
#
#                                     Examples:
#                                  **************
#                                 *    pause 1   *
#                                 *  pause 0.75  *
#                                  **************
#
 ###############################################################################
{
  if [[ "$1" != "0" ]]
  then
    pausevalue=$( rng "$1" )
  else
    pausevalue="0"
  fi
  sleep "$pausevalue"s

}
libtextfunctions+=("pause")
libtextvariables+=("pausevalue")

slowmsg()
  ##############################################################################
 #
# Displays strings in order on a per character basis with a very low time delay.
# This accepts 1 input.
# The first input should be to an string or array to display.
#
#                                    Examples:
#   /------------------------------------------------------------------------/
#  |                                As a string.                            |
#   \------------------------------------------------------------------------\
#    *******************/                                    \*****************
#     *                | slowmsg "This message will be slow." |                *
#    *******************\                                    /*****************
#   /------------------------------------------------------------------------/
#  |   As an array. All lines in the array will display in a slow manner.   |
#   \------------------------------------------------------------------------\
#    *                      |                           |                     *
#     *                      \ slowmsg "${welcome[@]}" /                       *
#      *                      \                       /
#       ************************************************************************
 #
  ##############################################################################
{
  if [[ "$commaspeed" = "" ]]
  then
    local commaspeed="0.45"
  fi
  if [[ "$periodspeed" = "" ]]
  then
    local periodspeed="0.7"
  fi
  if [[ "$speed" = "" ]]
  then
    local speed="0.05"
  fi
  if [[ "$linespeed" = "" ]]
  then
    local linespeed="0.3"
  fi
  if [[ "$rngseed" = "" ]]
  then
    local rngseed="0"
  fi

  messages=("$@")
  for message in "$@"
  do
    local lastword=${message##* }
    local message_length=${#message[0]}
    (( message_length-- ))

    for ((i=0; i<=$message_length; i++))
    # Cycle through all characters in the line until you hit the last character.
    # Print the charactrs and apply the approiate grammar pauses.
    do
      # Print the character out.
      if [[ ${msgcolors[$message]} != "" ]]
      then
        printf '%s' "${msgcolors[$message]}"
        break
      else
        printf '%s' "${message[@]:$i:1}"
      fi
      # Create seperate timing for comma and period.
      # Everything else runs at .05s
      if [[ ${message[@]:$i:1} = "," ]]
        # Comma character runs at .45s + .05s = .5s
      then
        pause "$commaspeed"
      elif [[ ${message[@]:$i:1} = "." ]] && [[ ${message[@]:$i+1:1} != "." ]] \
           && [[ ${message[@]:$i+1:1} = "" ]]
        # This logic stops any delay that would happen with multiple periods.
        # It does the apropriate delay for the last period of an elipsis.
        # Period character runs at .7s + .05s = .75s
      then
        pause "$periodspeed"
      fi
      # General Characters run at .05s
      pause "$speed"
    done
    local lastchar=$((${#lastword}-1))
    if [[ "$message" != "" ]] && [[ "$message" != " " ]] && [[ ! "$lastchar" ]]
    then
      printf " "
    fi
    # Print a new line at the end of each line. Also does a slight pause.
    if [[ $question != "yes" ]]
    then
      pause "$linespeed"
      if [[ $message != $'\033'* ]]
      then
        if (( "${#messages[@]}" > 1 ))
        then
          if [[ "$message" = "${messages[-2]}" ]] && [[ "${messages[-1]}" = "coloroff" ]]
          then
            printf '%s' "${msgcolors[coloroff]}"
          fi
        fi
        if [[ -n "$message" ]] && [[ -n ${msgcolors[$message]} ]]
        then
          continue
        else
          printf "\n"
        fi
      fi
    fi
  done
}
libtextfunctions+=("slowmsg")
libtextvariables+=("speed" "linespeed" "commaspeed" "periodspeed" "message")

fastmsg()
  ##############################################################################
 #
# Displays strings in order on a per word basis with a very low time delay.
# This accepts 1 input.
# The first input should be to an string or array to display.
#
#                                    Examples:
#   /------------------------------------------------------------------------/
#  |                                As a string.                            |
#   \------------------------------------------------------------------------\
#    *******************/                                    \*****************
#     *                | fastmsg "This message will be fast." |                *
#    *******************\                                    /*****************
#   /------------------------------------------------------------------------/
#  |     As an array. All lines in the array will be displayed quickly.     |
#   \------------------------------------------------------------------------\
#    *                     |                           |                      *
#     *                     \ fastmsg "${welcome[@]}" /                        *
#      *                     \                       /
#       ************************************************************************
#
 ###############################################################################
{

  if [[ "$commaspeed" = "" ]]
  then
    local commaspeed="0.42"
  fi
  if [[ "$periodspeed" = "" ]]
  then
    local periodspeed="0.67"
  fi
  if [[ "$speed" = "" ]]
  then
    local speed="0.08"
  fi
  if [[ "$linespeed" = "" ]]
  then
    local linespeed="0.3"
  fi
  if [[ "$rngseed" = "" ]]
  then
    local rngseed="0"
  fi

  messages=("$@")
  for string in "${messages[@]}"
  # This allows us to go through each string individually.
  do
    unset -v wordlist word
    wordindex=0 ## Word index keeps track of the number of words have collected.
    wordslen=${#string[0]} ## Checks the length of the string.
    for (( i=0; i<=$wordslen ; i++ ))
    ## Go through each character in the string.
    do
      if [[ "$word" = "" ]] && [[ "${string[@]:$i:1}" = " " ]]
      ## If the word variable is empty and the first character is a space
      ## Set word to 0.
      then
        word="0"
      elif [[ "$word" = "" ]] && [[ "${string[@]:$i:1}" != " " ]]
      ## If the first letter is not whitespace then it should be a word.
      then
        word="1"
      fi

      if [[ "$word" = "0" ]] && [[ "${string[@]:$i:1}" != " " ]]
      ## When we've run out of spaces, Change to a word and increment the index.
      then
        (( wordindex++ ))
        word="1"
      fi
      if [[ "$word" = "1" ]] && [[ "${string[@]:$i:1}" = " " ]]
      ## When we've ended the word and hit whitespace, Disable the word and
      ## increment the index.
      then
        (( wordindex++ ))
        word="0"
      fi
      ## Add the character to the wordlist.
      # shellcheck disable=SC2124
      wordlist[$wordindex]+="${string[@]:$i:1}"
    done
    ## Make sure the word variable is not initially set from the last logic set.
    unset word printindex
    printindex=0
    #printf '\n%s\n' "$printindex"
    for word in "${wordlist[@]}"
    ## Go through the words and print them with a time delay.
    do
      if [[ $word != "" ]]
        then
        if [[ ${msgcolors[$word]} != "" ]] && [[ "$word" = "${wordlist[*]}" ]]
        then
          printf '%s' "${msgcolors[$word]}"
        else
          printf '%s' "$word" # Print the word.
          pause "$speed" # .08 second pause per word.
        fi
      fi
      local lastchar=$((${#word}-1))
      if [[ ${word:$lastchar:1} = "," ]]
      ## This creates the comma grammar pause for fastmsg. Giving the same
      ## Pauses as slowmsg. .08 + .42 = .5 delay on a comma.
      then
        pause "$commaspeed"
      elif [[ ${word:$lastchar:1} = "." ]]
      ## Add the pause for period grammar for fastmsg. Giving the same pauses
      ## As slowmsg. .08 + .67 = .75 delay on a period.
      then
        pause "$periodspeed"
      fi

      # printf "$wordindex $printindex"

      if [[ "$wordindex" = "$printindex" ]] #[[ $word = "${wordlist[$wordindex]}" ]] &&
        # If we are on the last word of the line move to the next line.
        #   Then break out of the infinite for loop.
      then
        if [[ $question != "yes" ]]
          then
          pause "$linespeed"
          if [[ $word != $'\033'* ]]
          then
            if (( "${#messages[@]}" > 1 ))
            then
              if [[ "$string" = "${messages[-2]}" ]] && [[ "${messages[-1]}" = "coloroff" ]]
              then
                printf '%s' "${msgcolors[coloroff]}"
              fi
            fi
            # if [[ "$word" != "" ]] && [[ -z "${msgcolors[$word]}" ]]
            # ## ||
            #    ## [[ "$word" = " " ]] && [[ -z "${msgcolors[$word]}" ]]
            # ## This line is broken.
            # then
            #   printf "\n"
            if [[ -n "$word" ]] && [[  -n "${msgcolors[$word]}"  ]]
            then
              continue
            else
              printf "\n"
            fi
          fi
        fi
      fi
      (( printindex++ ))
    done
  done
}
libtextfunctions+=("fastmsg" "cleanup_libtext")
libtextvariables+=("string" "libtextfunctions" "libtextvariables" "commaspeed" "periodspeed" "speed" "linespeed" "wordlist" "word" "wordslen")

nodelaymsg()
  ##############################################################################
 #
#
# No delay on messages for quick display preference.
#
 ###############################################################################
{
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"

  fastmsg "$@"

  unset -v commaspeed periodspeed speed linespeed
}
libtextfunctions+=("nodelaymsg")


findlibtextconf()
## Finds lib-text.conf so msgspd can modify it.
{
  SP=$(readlink -f "${BASH_SOURCE[0]}")
  if [[ "$SP" = "$HOME"* ]]
  then
    conf=${SP%/*}
    conf=${conf%/*}
    conf+="/conf"
  else
    conf="/etc/orbos"
  fi

  libtextconf="$conf"/lib-text.conf
}
libtextvariables+=("libtextconf" "SP" "conf")
libtextfunctions+=("findlibtextconf")

setprogmsgcolors()
{
  unset progmsgarray inputcolor
  while IFS= read -r line
  do
    if [[ "$line" =~ [A-Z\ \!\,\.] ]] && ! [[ "$line" =~ [a-z] ]]
    then
      inputcolor="${syntaxcolors[capscolor]}"
      progmsgarray+=( "${msgcolors[$inputcolor]}" )
      progmsgarray+=( "$line" )
      progmsgarray+=( "${msgcolors[coloroff]}" )
    elif [[ "$line" =~ [A-Za-z\ \-] ]] && [[ "$line" = *"--"* ]]
    then
      inputcolor="${syntaxcolors[flags]}"
      progmsgarray+=( "${msgcolors[$inputcolor]}" )
      progmsgarray+=( "$line" )
      progmsgarray+=( "${msgcolors[coloroff]}" )
    else
      inputcolor="${syntaxcolors[main]}"
      progmsgarray+=( "${msgcolors[$inputcolor]}" )
      progmsgarray+=( "$line" )
      progmsgarray+=( "${msgcolors[coloroff]}" )
    fi
  done < <( "$@" )
}
libtextfunctions+=("setprogmsgcolor")
libtextvariables+=("progmsgarray" "inputcolor")

stripcolor()
{
  unset -v nocolorarray
  for entry in "${progmsgarray[@]}"
  do
    if [[ "$entry" =~ \[[0-9]m ]] ||
       [[ "$entry" =~ \[[0-9\;]*m ]]
    then
      continue
    else
      nocolorarray+=( "$entry" )
    fi
  done
  progmsgarray=( "${nocolorarray[@]}" )
}
libtextfunctions+=("stripcolor")

msgspd()
{
  setmsg="$1" ## Uses the first argument sent to the command as the switch.
  findlibtextconf ## Find the location of lib-text.conf to work on.
  ## Set the current lib-text.conf settings in the msgstatus array.
  msgstatus=($(head -n 18 "$libtextconf" | tail -n 3))
  if [[ "$setmsg" != "msg" ]] && [[ "$setmsg" != "s" ]] && \
     [[ "$setmsg" != "a" ]] && [[ "$setmsg" != "o" ]] && \
     [[ "$setmsg" != "m" ]] && [[ "$setmsg" != "g" ]]
    ## Ensure no duplicates happen via duplicate letters or words.
    ## Allowing duplicates would allow false positives and negatives.
  then
    for msgtype in "${msgstatus[@]}"
    ## Cycle through the msgstatus array.
    do
      if [[ "$msgtype" != '"$@"' ]]
      ## Ignore entries passing on command line arguments.
      then
        if [[ "$msgtype" = "#$setmsg"* ]]
        ## If the msg type is commented then we will uncomment it.
        then
          #printf '%s %s\n' "Uncommenting line:" "$msgtype"
          uncomment=${msgtype#\#}
          sed -i 's/'"$msgtype"'/'"$uncomment"'/g' "$libtextconf"
        fi
        if [[ "$msgtype" != "#"* ]] && [[ "$msgtype" != *"$setmsg"* ]]
          # If the msg type is uncommented and should be commented then we comment it.
        then
          # printf '%s %s\n' "Commenting line:" "$msgtype"
          sed -i 's/'"$msgtype"'/#'"$msgtype"'/g' "$libtextconf"
        fi
      fi
    done
  fi
  #head -n 18 lib-text.conf | tail -n 3
  if [[ $(type -t cleanup_libtextconf) ]]
    ## Unsets libtextconf before defining a new version if it is reloaded.
    then
    cleanup_libtextconf
  fi
  findlibtextconf
  . "$libtextconf"
}
libtextvariables+=("setmsg" "libtext" "msgstatus" "msgtype" "uncomment")
libtextfunctions+=("msgspd")

rngseed()
{
  setseed="$1"
  findlibtextconf
  seedline=($(head -n 22 "$libtextconf" | tail -n 1))
  printf '%s\n' "$seedline" | grep -E -e '[[:digit:]]'
  if [[ "$?" -eq 0 ]]
  then
    newseedline=${seedline%=*}
    newseedline="$newseedline""=""$setseed"
    #sed -i 's/'"$msgtype"'/#'"$msgtype"'/g' "$libtextconf"
    #sed -i 's/'"$msgtype"'/'"$uncomment"'/g' "$libtextconf"
    sed -i 's/'"$seedline"'/'"$newseedline"'/g' "$libtextconf"
  fi
}

cleanup_libtext()
## Unset all the functions and variables used by libtext.
{
  for function in "${libtextfunctions[@]}"
  ## Unset the functions.
  do
    unset -f "$function"
  done
  ## Unset the variables.
  libtextvariables+=("_" "var")
  for var in "${libtextvariables[@]}"
  do
    unset -v "$var"
  done
  exit 0
}
