#!/usr/bin/env bash

msg()
## Configures the msg function.
## The msg function will display what you want and then provide a newline
## Automatically.
{
  ## Only one of ##slowmsg, #fastmsg or ##nodelaymsg should be active at a time.
  ## Otherwise the result will be duplicated messages.
  ## If you do not like the default speeds for ##slowmsg or #fastmsg you can set
  ## your own with these values.
  #commaspeed="0.25" ## A value in seconds. Default: ##slowmsg=".45" #fastmsg=".42"
  #periodspeed="0.7" ## A value in seconds. Default: ##slowmsg=".7" #fastmsg=".67"
  #speed="0.05" ## A value in seconds. Default: ##slowmsg=".05" #fastmsg=".08"
  #linespeed="0.001" ## A value in seconds. Default: ##slowmsg=".3" #fastmsg=".3"
  slowmsg "$@"
  #fastmsg "$@"
  #nodelaymsg "$@"
}

# The rngseed determines the rng seed value. It should be 0-18.
declare -ig rngseed="8" # A higher seed value will cause more difference in each display.

rmsg()
## Performs the same as msg except it does a return before.
{
  msg " "
  msg "$@"
}

msgr()
## Performs the same as msg except it does a return line afterwards.
{
  msg "$@"
  msg " "
}

rmsgr()
## Performs the same as msg exept it does a return before and after.
{
  msg " "
  msg "$@"
  msg " "
}

qmsg()
## Configures the qmsg function. Its meant for questions.
## The qmsg function will display what you want without a newline.
## There are specific cases where qmsg will display your text properly where msg
## may mangle it.
{
  ## question yes/no should not be changed. This changes and then resets it.
  question="yes"
  msg "$@"
  # shellcheck disable=SC2034
  question="no"
}

qmsgr()
## Performs the same as qmsg except it does a return line afterwards.
{
  qmsg "$@"
  msg " "
}

rqmsg()
## Performs the same as qmsg except it does a return before.
{
  msg " "
  qmsg "$@"
}

rqmsgr()
## Performs the same as qmsg exept it does a return before and after.
{
  msg " "
  qmsg "$@"
  msg " "
}

pmsg()
{
  setprogmsgcolors "$@"
  msg "${progmsgarray[@]}"
}

declare -Ag msgcolors=(
  [coloroff]=$'\e[0m'

  # Standard colors.
  [black]=$'\e[0;30m'        # Black
  [red]=$'\e[0;31m'          # Red
  [green]=$'\e[0;32m'        # Green
  [yellow]=$'\e[0;33m'       # Yellow
  [blue]=$'\e[0;34m'         # Blue
  [purple]=$'\e[0;35m'       # Purple
  [cyan]=$'\e[0;36m'         # Cyan
  [white]=$'\e[0;37m'        # White

  # Underline
  [_black]=$'\e[4;30m'       # Black
  [_red]=$'\e[4;31m'         # Red
  [_green]=$'\e[4;32m'       # Green
  [_yellow]=$'\e[4;33m'      # Yellow
  [_blue]=$'\e[4;34m'        # Blue
  [_purple]=$'\e[4;35m'      # Purple
  [_cyan]=$'\e[4;36m'        # Cyan
  [_white]=$'\e[4;37m'       # White

  # Bold
  [Black]=$'\e[1;30m'       # Black
  [Red]=$'\e[1;31m'         # Red
  [Green]=$'\e[1;32m'       # Green
  [Yellow]=$'\e[1;33m'      # Yellow
  [Blue]=$'\e[1;34m'        # Blue
  [Purple]=$'\e[1;35m'      # Purple
  [Cyan]=$'\e[1;36m'        # Cyan
  [White]=$'\e[1;37m'       # White

  ## Bright colored text.
  [~black]=$'\e[0;90m'        # Black
  [~red]=$'\e[0;91m'          # Red
  [~green]=$'\e[0;92m'        # Green
  [~yellow]=$'\e[0;93m'       # Yellow
  [~blue]=$'\e[0;94m'         # Blue
  [~purple]=$'\e[0;95m'       # Purple
  [~cyan]=$'\e[0;96m'         # Cyan
  [~white]=$'\e[0;97m'        # White

  ## Bright colored bold text.
  [~Black]=$'\e[1;90m'        # Black
  [~Red]=$'\e[1;91m'          # Red
  [~Green]=$'\e[1;92m'        # Green
  [~Yellow]=$'\e[1;93m'       # Yellow
  [~Blue]=$'\e[1;94m'         # Blue
  [~Purple]=$'\e[1;95m'       # Purple
  [~Cyan]=$'\e[1;96m'         # Cyan
  [~White]=$'\e[1;97m'        # White

  ## Bright colored underlined text.
  [~_black]=$'\e[4;90m'        # Black
  [~_red]=$'\e[4;91m'          # Red
  [~_green]=$'\e[4;92m'        # Green
  [~_yellow]=$'\e[4;93m'       # Yellow
  [~_blue]=$'\e[4;94m'         # Blue
  [~_purple]=$'\e[4;95m'       # Purple
  [~_cyan]=$'\e[4;96m'         # Cyan
  [~_white]=$'\e[4;97m'        # White

  # Blinking text.
  [.black]=$'\e[5;30m'       # Black
  [.red]=$'\e[5;31m'         # Red
  [.green]=$'\e[5;32m'       # Green
  [.yellow]=$'\e[5;33m'      # Yellow
  [.blue]=$'\e[5;34m'        # Blue
  [.purple]=$'\e[5;35m'      # Purple
  [.cyan]=$'\e[5;36m'        # Cyan
  [.white]=$'\e[5;37m'       # White

  # Blinking bright text.
  [~.black]=$'\e[5;90m'       # Black
  [~.red]=$'\e[5;91m'         # Red
  [~.green]=$'\e[5;92m'       # Green
  [~.yellow]=$'\e[5;93m'      # Yellow
  [~.blue]=$'\e[5;94m'        # Blue
  [~.purple]=$'\e[5;95m'      # Purple
  [~.cyan]=$'\e[5;96m'        # Cyan
  [~.white]=$'\e[5;97m'       # White

  # Background
  [onblack]=$'\e[40m'       # Black
  [onred]=$'\e[41m'         # Red
  [ongreen]=$'\e[42m'       # Green
  [onyellow]=$'\e[43m'      # Yellow
  [onblue]=$'\e[44m'        # Blue
  [onpurple]=$'\e[45m'      # Purple
  [oncyan]=$'\e[46m'        # Cyan
  [onwhite]=$'\e[47m'       # White

  # Bright background
  [on~black]=$'\e[100m'       # Black
  [on~red]=$'\e[101m'         # Red
  [on~green]=$'\e[102m'       # Green
  [on~yellow]=$'\e[103m'      # Yellow
  [on~blue]=$'\e[104m'        # Blue
  [on~purple]=$'\e[105m'      # Purple
  [on~cyan]=$'\e[106m'        # Cyan
  [on~white]=$'\e[107m'       # White

  ## This adds all ansi color codes to lib-text.
  [0]=$'\e[38;5;0m'
  [1]=$'\e[38;5;1m'
  [2]=$'\e[38;5;2m'
  [3]=$'\e[38;5;3m'
  [4]=$'\e[38;5;4m'
  [5]=$'\e[38;5;5m'
  [6]=$'\e[38;5;6m'
  [7]=$'\e[38;5;7m'
  [8]=$'\e[38;5;8m'
  [9]=$'\e[38;5;9m'
  [10]=$'\e[38;5;10m'
  [11]=$'\e[38;5;11m'
  [12]=$'\e[38;5;12m'
  [13]=$'\e[38;5;13m'
  [14]=$'\e[38;5;14m'
  [15]=$'\e[38;5;15m'
  [16]=$'\e[38;5;16m'
  [17]=$'\e[38;5;17m'
  [18]=$'\e[38;5;18m'
  [19]=$'\e[38;5;19m'
  [20]=$'\e[38;5;20m'
  [21]=$'\e[38;5;21m'
  [22]=$'\e[38;5;22m'
  [23]=$'\e[38;5;23m'
  [24]=$'\e[38;5;24m'
  [25]=$'\e[38;5;25m'
  [26]=$'\e[38;5;26m'
  [27]=$'\e[38;5;27m'
  [28]=$'\e[38;5;28m'
  [29]=$'\e[38;5;29m'
  [30]=$'\e[38;5;30m'
  [31]=$'\e[38;5;31m'
  [32]=$'\e[38;5;32m'
  [33]=$'\e[38;5;33m'
  [34]=$'\e[38;5;34m'
  [35]=$'\e[38;5;35m'
  [36]=$'\e[38;5;36m'
  [37]=$'\e[38;5;37m'
  [38]=$'\e[38;5;38m'
  [39]=$'\e[38;5;39m'
  [40]=$'\e[38;5;40m'
  [41]=$'\e[38;5;41m'
  [42]=$'\e[38;5;42m'
  [43]=$'\e[38;5;43m'
  [44]=$'\e[38;5;44m'
  [45]=$'\e[38;5;45m'
  [46]=$'\e[38;5;46m'
  [47]=$'\e[38;5;47m'
  [48]=$'\e[38;5;48m'
  [49]=$'\e[38;5;49m'
  [50]=$'\e[38;5;50m'
  [51]=$'\e[38;5;51m'
  [52]=$'\e[38;5;52m'
  [53]=$'\e[38;5;53m'
  [54]=$'\e[38;5;54m'
  [55]=$'\e[38;5;55m'
  [56]=$'\e[38;5;56m'
  [57]=$'\e[38;5;57m'
  [58]=$'\e[38;5;58m'
  [59]=$'\e[38;5;59m'
  [60]=$'\e[38;5;60m'
  [61]=$'\e[38;5;61m'
  [62]=$'\e[38;5;62m'
  [63]=$'\e[38;5;63m'
  [64]=$'\e[38;5;64m'
  [65]=$'\e[38;5;65m'
  [66]=$'\e[38;5;66m'
  [67]=$'\e[38;5;67m'
  [68]=$'\e[38;5;68m'
  [69]=$'\e[38;5;69m'
  [70]=$'\e[38;5;70m'
  [71]=$'\e[38;5;71m'
  [72]=$'\e[38;5;72m'
  [73]=$'\e[38;5;73m'
  [74]=$'\e[38;5;74m'
  [75]=$'\e[38;5;75m'
  [76]=$'\e[38;5;76m'
  [77]=$'\e[38;5;77m'
  [78]=$'\e[38;5;78m'
  [79]=$'\e[38;5;79m'
  [80]=$'\e[38;5;80m'
  [81]=$'\e[38;5;81m'
  [82]=$'\e[38;5;82m'
  [83]=$'\e[38;5;83m'
  [84]=$'\e[38;5;84m'
  [85]=$'\e[38;5;85m'
  [86]=$'\e[38;5;86m'
  [87]=$'\e[38;5;87m'
  [88]=$'\e[38;5;88m'
  [89]=$'\e[38;5;89m'
  [90]=$'\e[38;5;90m'
  [91]=$'\e[38;5;91m'
  [92]=$'\e[38;5;92m'
  [93]=$'\e[38;5;93m'
  [94]=$'\e[38;5;94m'
  [95]=$'\e[38;5;95m'
  [96]=$'\e[38;5;96m'
  [97]=$'\e[38;5;97m'
  [98]=$'\e[38;5;98m'
  [99]=$'\e[38;5;99m'
  [100]=$'\e[38;5;100m'
  [101]=$'\e[38;5;101m'
  [102]=$'\e[38;5;102m'
  [103]=$'\e[38;5;103m'
  [104]=$'\e[38;5;104m'
  [105]=$'\e[38;5;105m'
  [106]=$'\e[38;5;106m'
  [107]=$'\e[38;5;107m'
  [108]=$'\e[38;5;108m'
  [109]=$'\e[38;5;109m'
  [110]=$'\e[38;5;110m'
  [111]=$'\e[38;5;111m'
  [112]=$'\e[38;5;112m'
  [113]=$'\e[38;5;113m'
  [114]=$'\e[38;5;114m'
  [115]=$'\e[38;5;115m'
  [116]=$'\e[38;5;116m'
  [117]=$'\e[38;5;117m'
  [118]=$'\e[38;5;118m'
  [119]=$'\e[38;5;119m'
  [120]=$'\e[38;5;120m'
  [121]=$'\e[38;5;121m'
  [122]=$'\e[38;5;122m'
  [123]=$'\e[38;5;123m'
  [124]=$'\e[38;5;124m'
  [125]=$'\e[38;5;125m'
  [126]=$'\e[38;5;126m'
  [127]=$'\e[38;5;127m'
  [128]=$'\e[38;5;128m'
  [129]=$'\e[38;5;129m'
  [130]=$'\e[38;5;130m'
  [131]=$'\e[38;5;131m'
  [132]=$'\e[38;5;132m'
  [133]=$'\e[38;5;133m'
  [134]=$'\e[38;5;134m'
  [135]=$'\e[38;5;135m'
  [136]=$'\e[38;5;136m'
  [137]=$'\e[38;5;137m'
  [138]=$'\e[38;5;138m'
  [139]=$'\e[38;5;139m'
  [140]=$'\e[38;5;140m'
  [141]=$'\e[38;5;141m'
  [142]=$'\e[38;5;142m'
  [143]=$'\e[38;5;143m'
  [144]=$'\e[38;5;144m'
  [145]=$'\e[38;5;145m'
  [146]=$'\e[38;5;146m'
  [147]=$'\e[38;5;147m'
  [148]=$'\e[38;5;148m'
  [149]=$'\e[38;5;149m'
  [150]=$'\e[38;5;150m'
  [151]=$'\e[38;5;151m'
  [152]=$'\e[38;5;152m'
  [153]=$'\e[38;5;153m'
  [154]=$'\e[38;5;154m'
  [155]=$'\e[38;5;155m'
  [156]=$'\e[38;5;156m'
  [157]=$'\e[38;5;157m'
  [158]=$'\e[38;5;158m'
  [159]=$'\e[38;5;159m'
  [160]=$'\e[38;5;160m'
  [161]=$'\e[38;5;161m'
  [162]=$'\e[38;5;162m'
  [163]=$'\e[38;5;163m'
  [164]=$'\e[38;5;164m'
  [165]=$'\e[38;5;165m'
  [166]=$'\e[38;5;166m'
  [167]=$'\e[38;5;167m'
  [168]=$'\e[38;5;168m'
  [169]=$'\e[38;5;169m'
  [170]=$'\e[38;5;170m'
  [171]=$'\e[38;5;171m'
  [172]=$'\e[38;5;172m'
  [173]=$'\e[38;5;173m'
  [174]=$'\e[38;5;174m'
  [175]=$'\e[38;5;175m'
  [176]=$'\e[38;5;176m'
  [177]=$'\e[38;5;177m'
  [178]=$'\e[38;5;178m'
  [179]=$'\e[38;5;179m'
  [180]=$'\e[38;5;180m'
  [181]=$'\e[38;5;181m'
  [182]=$'\e[38;5;182m'
  [183]=$'\e[38;5;183m'
  [184]=$'\e[38;5;184m'
  [185]=$'\e[38;5;185m'
  [186]=$'\e[38;5;186m'
  [187]=$'\e[38;5;187m'
  [188]=$'\e[38;5;188m'
  [189]=$'\e[38;5;189m'
  [190]=$'\e[38;5;190m'
  [191]=$'\e[38;5;191m'
  [192]=$'\e[38;5;192m'
  [193]=$'\e[38;5;193m'
  [194]=$'\e[38;5;194m'
  [195]=$'\e[38;5;195m'
  [196]=$'\e[38;5;196m'
  [197]=$'\e[38;5;197m'
  [198]=$'\e[38;5;198m'
  [199]=$'\e[38;5;199m'
  [200]=$'\e[38;5;200m'
  [201]=$'\e[38;5;201m'
  [202]=$'\e[38;5;202m'
  [203]=$'\e[38;5;203m'
  [204]=$'\e[38;5;204m'
  [205]=$'\e[38;5;205m'
  [206]=$'\e[38;5;206m'
  [207]=$'\e[38;5;207m'
  [208]=$'\e[38;5;208m'
  [209]=$'\e[38;5;209m'
  [210]=$'\e[38;5;210m'
  [211]=$'\e[38;5;211m'
  [212]=$'\e[38;5;212m'
  [213]=$'\e[38;5;213m'
  [214]=$'\e[38;5;214m'
  [215]=$'\e[38;5;215m'
  [216]=$'\e[38;5;216m'
  [217]=$'\e[38;5;217m'
  [218]=$'\e[38;5;218m'
  [219]=$'\e[38;5;219m'
  [220]=$'\e[38;5;220m'
  [221]=$'\e[38;5;221m'
  [222]=$'\e[38;5;222m'
  [223]=$'\e[38;5;223m'
  [224]=$'\e[38;5;224m'
  [225]=$'\e[38;5;225m'
  [226]=$'\e[38;5;226m'
  [227]=$'\e[38;5;227m'
  [228]=$'\e[38;5;228m'
  [229]=$'\e[38;5;229m'
  [230]=$'\e[38;5;230m'
  [231]=$'\e[38;5;231m'
  [232]=$'\e[38;5;232m'
  [233]=$'\e[38;5;233m'
  [234]=$'\e[38;5;234m'
  [235]=$'\e[38;5;235m'
  [236]=$'\e[38;5;236m'
  [237]=$'\e[38;5;237m'
  [238]=$'\e[38;5;238m'
  [239]=$'\e[38;5;239m'
  [240]=$'\e[38;5;240m'
  [241]=$'\e[38;5;241m'
  [242]=$'\e[38;5;242m'
  [243]=$'\e[38;5;243m'
  [244]=$'\e[38;5;244m'
  [245]=$'\e[38;5;245m'
  [246]=$'\e[38;5;246m'
  [247]=$'\e[38;5;247m'
  [248]=$'\e[38;5;248m'
  [249]=$'\e[38;5;249m'
  [250]=$'\e[38;5;250m'
  [251]=$'\e[38;5;251m'
  [252]=$'\e[38;5;252m'
  [253]=$'\e[38;5;253m'
  [254]=$'\e[38;5;254m'
  [255]=$'\e[38;5;255m'
  [clear]=$'\e[39m'

)

declare -Ag syntaxcolors=(
  [capscolor]="~Green"
  [flags]="~Purple"
  [main]="~Blue"
)

cleanup_libtextconf()
{
  unset -f msg qmsg rmsg msgr rmsgr qmsgr rqmsg rqmsgr cleanup_libtextconf
  unset -v question msgcolors _
}
